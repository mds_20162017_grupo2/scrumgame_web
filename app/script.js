(function() {
    'use strict';

    const RANDOM_SIZE = 3;
    const INITIAL_BUDGET = 15000;

    var data;

    var motivation = 80;
    var budget = INITIAL_BUDGET;
    var effort = 0;
    var satisfaction = 80;
    var resources = 0;

    var historicMotivation = []; //{meta: 'motivação', value: 1 }
    var historicSatisfaction = [];
    var historicBudget = [];

    var historicEffort = [];
    var historicResources = [];
    
    var page = 0;

    var gameEnded = false;

    initialize();

    function initialize() {
        $.getJSON("app/data.json", function(json) {
            data = json;
            buildText();
        });
        $('#next').on('click', goNextQuestion);
        $('[data-toggle="tooltip"]').tooltip();
        $('#restart').on('click', restart);
        
    }
    
    function buildText() {
        writeParameters();
        writeTitle();
        writeInfo();
        writeQuestion();
        writeAnswers();
        updateProgressBar();
    }

    function writeParameters() {
        $('.js-motivation').html(motivation + '%');
        $('.js-budget').html(budget);
        $('.js-effort').html(effort);
        $('.js-resources').html(resources);
        $('.js-satisfaction').html(satisfaction + '%');
    }

    function writeTitle() {
        $('.js-title').html('<h3>'+data.pages[page].title+'</h3>');
    }

    function writeInfo() {
        $('.js-info').html('<p>'+data.pages[page].info+'</p>');
    }

    function writeQuestion() {
        $('.js-question').html('<p>'+data.pages[page].question+'</p>');
    }

    function writeAnswers() {
        // clean
        $('.js-answers-form').html('');

        var size = data.pages[page].answers.length;

        if(size > RANDOM_SIZE) {
            console.log('RANDOMIZE ANSWERS');
            do {
                data.pages[page].answers.splice(getRandomArbitrary(data.pages[page].answers.length), 1);
                size = data.pages[page].answers.length;
            }while(size > RANDOM_SIZE);
        }

        for (var i = 0; i < size; i++) {
            $('.js-answers-form').append('<label class="radio-inline"><input type="radio" value="' + i + '" name="awnswer"/>'+data.pages[page].answers[i].description+'</label><br/>');
        }
    }

    function updateProgressBar() {
        $('.js-progress-bar').html((page + 1) +  '/' + data.pages.length + ' - ' + data.pages[page].tag);
        $('.js-progress-bar').css({"width" : (((page + 1)*100)/data.pages.length) + "%"});
    }

    function getRandomArbitrary(max) {
        return Math.floor(Math.random() * max);
    }
    
    function verifyAnswer() {

        //verificar qual foi a resposta
        var choiceIndex = $('.js-answers-form').find(":checked").val();

        //fazer as contas o choiceIndex é a resposta que a pessoa escolheu
        if(choiceIndex === undefined) {
            return false;  
        } 

        affectParameters(choiceIndex);

        return true;
    }

    function saveParamsHistory() {

        historicMotivation.push({
            meta    :   'Motivação',
            value   :   motivation
        });

        historicSatisfaction.push({
            meta    :   'Satisfação',
            value   :   satisfaction
        });

        historicBudget.push({
            meta    :   'Orçamento',
            value   :   budget
        });

        historicEffort.push({
            meta    :   'Esforço',
            value   :   effort
        });
    }

    function affectParameters(choiceIndex) {

        clearParamsFeedback();

        let size = data.pages[page].answers[choiceIndex].AffectedParameters.length;
        for (let i = 0; i < size; i++) {
            if (data.pages[page].answers[choiceIndex].AffectedParameters[i].paramName === 'budget') {
                setFeedbackToParams(budget,budget + data.pages[page].answers[choiceIndex].AffectedParameters[i].value, 'budget');
                budget += data.pages[page].answers[choiceIndex].AffectedParameters[i].value;

                if(budget < 0) {
                    budget = 0;
                    gameEnded = true;
                }

            } else if (data.pages[page].answers[choiceIndex].AffectedParameters[i].paramName === 'satisfaction') {
                setFeedbackToParams(satisfaction, satisfaction + data.pages[page].answers[choiceIndex].AffectedParameters[i].value, 'satisfaction');
                satisfaction += data.pages[page].answers[choiceIndex].AffectedParameters[i].value;

                if(satisfaction > 100) {
                    satisfaction = 100;
                } else if (satisfaction <= 0) {
                    satisfaction = 0;
                    gameEnded = true;
                }

            } else if (data.pages[page].answers[choiceIndex].AffectedParameters[i].paramName === 'motivation') {
                setFeedbackToParams(motivation, motivation + data.pages[page].answers[choiceIndex].AffectedParameters[i].value, 'motivation');
                motivation += data.pages[page].answers[choiceIndex].AffectedParameters[i].value;

                if(motivation > 100) {
                    motivation = 100;
                } else if (motivation <= 0) {
                    motivation = 0;
                    gameEnded = true;
                }

            } else if (data.pages[page].answers[choiceIndex].AffectedParameters[i].paramName === 'effort') {
                setFeedbackToParams(effort, effort + data.pages[page].answers[choiceIndex].AffectedParameters[i].value, 'effort');
                effort += data.pages[page].answers[choiceIndex].AffectedParameters[i].value;

            } else if (data.pages[page].answers[choiceIndex].AffectedParameters[i].paramName === 'resources') {
                setFeedbackToParams(resources, resources + data.pages[page].answers[choiceIndex].AffectedParameters[i].value, 'resources');
                resources += data.pages[page].answers[choiceIndex].AffectedParameters[i].value;
            }
        }

        saveParamsHistory();
    }

    function setFeedbackToParams(oldParam, newParam, paramName) {

        if(newParam > oldParam){
            $('.js-up-arrow-' + paramName).removeClass('hidden');
        } else if(newParam < oldParam) {
            $('.js-down-arrow-' + paramName).removeClass('hidden');
        }
    }

    function clearParamsFeedback() {
        
        $('.js-down-arrow-motivation').addClass('hidden');
        $('.js-down-arrow-budget').addClass('hidden');
        $('.js-down-arrow-effort').addClass('hidden');
        $('.js-down-arrow-resources').addClass('hidden');
        $('.js-down-arrow-satisfaction').addClass('hidden');
        
        $('.js-up-arrow-motivation').addClass('hidden');
        $('.js-up-arrow-budget').addClass('hidden');
        $('.js-up-arrow-effort').addClass('hidden');
        $('.js-up-arrow-resources').addClass('hidden');
        $('.js-up-arrow-satisfaction').addClass('hidden');
    }

    //Verify if is the last page or params are negative
    function endOfGame() {

        if(gameEnded){
            return true;
        }

        let nextPage = page + 1;

        if(nextPage >= data.pages.length) {
            gameEnded = true;
            return true;
        } else {
            return false;
        }
    }

    function goNextQuestion() {
        console.log('NEXT');
        if(!gameEnded && verifyAnswer()) {
                
            if(endOfGame()) {
                //handle end of game here
                writeParameters();
                addRestartGame();
                doGraphs();
                console.log('END');
            } else {
                page++;
                buildText();
            }
        } 
    }

    function doGraphs() {
        $('#myModal').modal();
        $('#restart').off('click', restart);
        $('#restart').on('click', restart);
        
        let labels = [];
        let size = data.pages.length;
        for (let i = 1; i <= size; i++) {
            labels.push(i);
        }

        new Chartist.Line('#chart1', {
            labels: labels,
            series: [
                historicMotivation,
                historicSatisfaction
            ]},{
            fullWidth: true,
            chartPadding: {
                right: 50,
                left: 50
            },
            axisY: {
                onlyInteger: true
            },
            plugins: [
                Chartist.plugins.tooltip(),
                Chartist.plugins.ctAxisTitle({
                        axisX: {
                            axisTitle: 'Passos',
                            axisClass: 'ct-axis-title',
                            offset: {
                                x: 0,
                                y: 0
                            },
                            textAnchor: 'middle'
                        },
                        axisY: {
                            axisTitle: 'Percentagem',
                            axisClass: 'ct-axis-title',
                            offset: {
                                x: 0,
                                y: 0
                            },
                            flipTitle: false
                        }
                    })
                ]
            });
        new Chartist.Line('#chart2', {
            labels: labels,
            series: [
                historicBudget
            ]},{
            fullWidth: true,
            chartPadding: {
                right: 50,
                left: 50
            },
            axisY: {
                onlyInteger: true
            },
            plugins: [
                Chartist.plugins.tooltip(),
                Chartist.plugins.ctAxisTitle({
                        axisX: {
                            axisTitle: 'Passos',
                            axisClass: 'ct-axis-title',
                            offset: {
                                x: 0,
                                y: 0
                            },
                            textAnchor: 'middle'
                        },
                        axisY: {
                            axisTitle: 'Orçamento (€)',
                            axisClass: 'ct-axis-title',
                            offset: {
                                x: 0,
                                y: 0
                            },
                            flipTitle: false
                        }
                    })
                ]
            });
        new Chartist.Line('#chart3', {
            labels: labels,
            series: [
                historicEffort
            ]},{
            fullWidth: true,
            chartPadding: {
                right: 50,
                left: 50
            },
            axisY: {
                onlyInteger: true
            },
            plugins: [
                Chartist.plugins.tooltip(),
                Chartist.plugins.ctAxisTitle({
                        axisX: {
                            axisTitle: 'Passos',
                            axisClass: 'ct-axis-title',
                            offset: {
                                x: 0,
                                y: 0
                            },
                            textAnchor: 'middle'
                        },
                        axisY: {
                            axisTitle: 'Esforço (horas)',
                            axisClass: 'ct-axis-title',
                            offset: {
                                x: 0,
                                y: 0
                            },
                            flipTitle: false
                        }
                    })
                ]
            });
    }

    function addRestartGame() {
        $('#next').hide();
        $('#restart').show();
    }

    function restart() {
        window.location.reload();
    }

}());